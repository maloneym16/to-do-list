from django.urls import path
from tasks.views import create_task, show_my_tasks
from projects.views import list_projects


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("list/", list_projects, name="list_projects"),
]
